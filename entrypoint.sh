#!/bin/sh
set -e  # Exit immediately if a command exits with a non-zero status
set -x  # all executed commands are printed to the terminal.

export PYTHONUNBUFFERED=0

python3 manage.py makemigrations --noinput
python3 manage.py migrate
python3 manage.py create_sa
python3 manage.py runserver 0:8000

exec "$@"
