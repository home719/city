FROM python:3.9.0-alpine3.12 as dependencies

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

COPY . /opt/city/
WORKDIR /opt/city/

RUN python -m pip install --upgrade pip

RUN apk update && \
    apk add --virtual build-deps gcc python3-dev musl-dev && \
    apk add postgresql-dev

RUN python -m pip install -r /opt/city/requirements.txt

RUN chmod +x /opt/city/entrypoint.sh

EXPOSE 8000

ENTRYPOINT ["sh", "/opt/city/entrypoint.sh"]
