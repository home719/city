from django.urls import path, re_path

from . import views

app_name = 'maps'

urlpatterns = [
    path('', views.CityApi.as_view()),
    path('import/', views.CityImportApi.as_view({'get': 'get'})),
    path('find/', views.CityFindApi.as_view({'post': 'post'})),
    re_path(r'^find/([0-9]+)/$', views.CityDistanceApi.as_view()),
]
