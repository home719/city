from django.db import models
# from mptt.models import MPTTModel
import pytz


class City(models.Model):
    address = models.CharField(max_length=256)
    postal_code = models.CharField(max_length=8, blank=True)
    country = models.CharField(max_length=32,)
    federal_district = models.CharField(max_length=32, blank=True)
    region_type = models.CharField(max_length=32, blank=True)
    region = models.CharField(
        max_length=64,
        default='',
        blank=True
    )
    area_type = models.CharField(max_length=32, blank=True)
    area = models.CharField(
        max_length=64,
        default='',
        blank=True
    )
    city_type = models.CharField(max_length=32, blank=True)
    city = models.CharField(
        max_length=64,
        default='',
        blank=True
    )
    settlement_type = models.CharField(max_length=32, blank=True)
    settlement = models.CharField(
        max_length=64,
        default='',
        blank=True
    )
    kladr_id = models.CharField(
        max_length=32,
        blank=False
    )
    fias_id = models.CharField(
        max_length=64,
        blank=False
    )
    fias_level = models.IntegerField()
    capital_marker = models.IntegerField()
    okato = models.CharField(
        max_length=32,
        blank=False
    )
    oktmo = models.CharField(
        max_length=32,
        blank=False
    )
    tax_office = models.CharField(
        max_length=16,
        blank=False
    )

    TIMEZONES = tuple(zip(pytz.all_timezones, pytz.all_timezones))
    timezone = models.CharField(max_length=32, choices=TIMEZONES, default='UTC+3')
    geo_lat = models.DecimalField(max_digits=12, decimal_places=9)
    geo_lon = models.DecimalField(max_digits=12, decimal_places=9)
    population = models.CharField(
        max_length=16,
        blank=False
    )
    foundation_year = models.CharField(
        max_length=32,
        blank=True
    )
