import os
import shutil
from pathlib import Path

from rest_framework import status
from rest_framework import generics, viewsets
from rest_framework.response import Response

from .models import City
from .serializers import CitySerializer, CityFindSerializer, CityDistanceSerializer
from .services import import_data_from_file, address_find, address_find_distance


class CityApi(generics.ListAPIView):
    queryset = City.objects.all()
    serializer_class = CitySerializer

    def get_queryset(self):
        queryset = City.objects.filter()
        return queryset


class CityImportApi(viewsets.ViewSet):
    serializer_class = CitySerializer
    http_method_names = ['get', 'head']

    def get_queryset(self):
        queryset = City.objects.all()
        return queryset

    def get(self, request, *args, **kwargs):
        file_path = Path(__file__).resolve().parent.parent
        shutil.copyfile(str(file_path) + '/city.csv', str(file_path) + '/maps_city.csv')
        ret = import_data_from_file(str(file_path) + '/maps_city.csv')
        os.remove(str(file_path) + '/maps_city.csv')
        return Response(ret, status=status.HTTP_202_ACCEPTED)


class CityFindApi(viewsets.ViewSet):
    serializer_class = CityFindSerializer
    http_method_names = ['post', 'option', 'get', 'head']

    def get_queryset(self):
        queryset = City.objects.filter()
        return queryset

    def post(self, request, *args, **kwargs):
        _address = request.data['address']
        ret = address_find(_address)
        res = ret['result']
        latitude = ret['geo_lat']
        longitude = ret['geo_lon']
        rets = {'Объект': res, 'Широта': latitude, 'Долгота': longitude}
        request.session['latitude'] = latitude
        request.session['longitude'] = longitude
        return Response(rets, status=status.HTTP_202_ACCEPTED)


class CityDistanceApi(generics.ListAPIView):
    serializer_class = CityDistanceSerializer
    http_method_names = ['get', 'get']

    def get_queryset(self):
        queryset = City.objects.filter()
        return queryset

    def get(self, request, *args, **kwargs):
        dist = int(args[0])
        latitude = request.session.get('latitude')
        longitude = request.session.get('longitude')
        spis = address_find_distance(lat=latitude, lon=longitude, dist=dist)
        rets = {'Расстояние': dist, 'Широта': latitude, 'Долгота': longitude, 'Список городов на расстоянии': spis}
        return Response(rets, status=status.HTTP_202_ACCEPTED)
