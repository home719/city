from import_export import resources

from .models import City


class CityResource(resources.ModelResource):
    class Meta:
        model = City
        import_id_fields = ('address',)
        skip_unchanged = True
        report_skipped = True
