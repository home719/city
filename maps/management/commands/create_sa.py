from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model


class Command(BaseCommand):
    help = 'Create django-admin superuser or change password to default'

    def handle(self, *args, **options):
        username = 'postgres'
        email = 'p@m.ru'
        password = 'postgres'

        if not password:
            print('Create superuser skipped!')
            return

        user, created = get_user_model().objects.\
            get_or_create(username=username, defaults={'email': email, 'is_staff': True, 'is_superuser': True})
        user.set_password(password)
        user.save()

        if created:
            print('Superuser created! Use {0} {1}'.format(username, password))
        else:
            print('Superuser password changed! Use {0} {1}'.format(username, password))
