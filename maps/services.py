import os
import tablib
import csv
from geopy.distance import geodesic
from import_export import resources, results
from dadata import Dadata

from django.apps import apps
from django.db.models import Max, Min

from .resources import CityResource
from .models import City


REFERENCE_RESOURCES = (
    CityResource,
)


def address_find(address: str):
    token = "559678f4f4763196acd16e3eb4f968af3d57ba37"
    secret = "534a58d491211dd736545f31fc0fab1779ed9316"
    dada = Dadata(token, secret)
    # dada = Dadata(token)
    result = dada.clean("address", address)
    return result


def address_find_distance(lat, lon, dist: int) -> list:
    list_city = []
    max_id = City.objects.all().aggregate(Max('id'))['id__max']
    min_id = City.objects.all().aggregate(Min('id'))['id__min']
    center = (lat, lon)
    for _id in range(min_id, max_id + 1):
        geo_lat = City.objects.get(id=_id).geo_lat
        geo_lon = City.objects.get(id=_id).geo_lon
        _center = (geo_lat, geo_lon)
        if geodesic(center, _center).km <= dist:
            list_city.append(City.objects.get(id=_id).address)
    return list_city


def get_app_model_by_file_name(file_name: str):
    app_label, model_name = file_name.split('_')
    print(app_label, model_name)
    return apps.get_model(app_label=app_label, model_name=model_name)


def find_project_resource(database_model):
    for item in (*REFERENCE_RESOURCES, ):
        if item._meta.model == database_model:
            return item
    return resources.modelresource_factory(model=database_model)


def get_model_resource_instance_by_file_name(file_name: str):
    database_model = get_app_model_by_file_name(file_name)
    model_resource = find_project_resource(database_model)
    return model_resource()


def load_data_from_csv_file(file_path: str) -> str:
    file_name_with_extension = os.path.basename(file_path)
    file_name = os.path.splitext(file_name_with_extension)[0]
    model_resource = get_model_resource_instance_by_file_name(file_name)
    # model = model_resource._meta.model

    csv_file = open(file_path, 'r')
    csv_reader = csv.reader(csv_file, delimiter=',')
    header = next(csv_reader)
    dataset = tablib.Dataset(headers=header)
    if header is not None:
        for row in csv_reader:
            dataset.append(row)
    csv_file.close()

    result: results.Result = model_resource.import_data(dataset, dry_run=True)

    if not result.has_errors() and not result.has_validation_errors():
        result = model_resource.import_data(dataset, dry_run=False)
        print(result)
        return 'result'

    return file_path


def import_data_from_file(file_path: str):
    result = load_data_from_csv_file(file_path)
    return result
