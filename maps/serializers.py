from rest_framework import serializers

from .models import City


class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = '__all__'


class CityFindSerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = ('address', )


class CityDistanceSerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = ('address', 'geo_lat', 'geo_lon')
