# CITY

1. Build project in docker-compose
```bash
    docker-compose up -d --build
```
2. Начало
```
    http://localhost:8000/api/maps
```

3. Загрузка csv-файла
```
    http://localhost:8000/api/maps/import/
```
4. Поиск по названию населенного пункта, улицы etc
```
    http://localhost:8000/api/maps/find/
```
5. Получение списка городов, находящихся на расстоянии не превышающем NNN километров
``` 
    http://localhost:8000/api/maps/find/NNN
```

